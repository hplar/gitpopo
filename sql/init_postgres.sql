-- create namespaces table
CREATE TABLE IF NOT EXISTS namespaces
(
    id serial PRIMARY KEY,
    name VARCHAR(256) UNIQUE NOT NULL
);

-- create users table
CREATE TABLE IF NOT EXISTS users
(
  id SERIAL,
  login character varying(256) NOT NULL,
  passwd character varying(256) NOT NULL,
  is_superuser boolean NOT NULL DEFAULT false,
  disabled boolean NOT NULL DEFAULT false,
  identity character varying(128),
  namespace_id INTEGER REFERENCES namespaces(id) ON DELETE CASCADE,
  CONSTRAINT user_pkey PRIMARY KEY (id),
  CONSTRAINT user_login_key UNIQUE (login)
);

-- and permissions
CREATE TABLE IF NOT EXISTS permissions
(
id SERIAL,
user_id integer NOT NULL,
perm_name character varying(64) NOT NULL,
CONSTRAINT permission_pkey PRIMARY KEY (id),
CONSTRAINT user_permission_fkey FOREIGN KEY (user_id)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE
);

-- create bloomfilters table
CREATE TABLE IF NOT EXISTS bloomfilters
(
    id serial PRIMARY KEY,
    namespace_id INTEGER REFERENCES namespaces(id) ON DELETE CASCADE UNIQUE,
    bitarray bit varying(100000),
    error_rate NUMERIC(5, 5) NOT NULL,
    capacity INTEGER NOT NULL,
    used INTEGER NOT NULL
);
