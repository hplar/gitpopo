FROM hpl4r/python:alpine as base

RUN apk add postgresql-client

COPY requirements/base.txt base.txt
RUN pip3 install -r base.txt

RUN mkdir /app
COPY . /app

WORKDIR /app
