import aiohttp
import gino
import pytest
from gino import Gino
from gitpopo.server_common import build_application, configure
from pybloom_live import BloomFilter


@pytest.fixture
async def app(loop) -> aiohttp.web_app.Application:
    app: aiohttp.web_app.Application = await build_application(loop=loop)

    return app


@pytest.fixture
async def mock_bloomfilter() -> BloomFilter:
    bloomfilter: BloomFilter = BloomFilter(capacity=1000, error_rate=0.01)

    for secret in ['s3CrT', 'sUchSecR3tMuchwOW']:
        bloomfilter.add(secret)

    return bloomfilter


@pytest.fixture()
async def db(mock_bloomfilter) -> gino.api.Gino:
    db = Gino()

    config = await configure()
    bind_address: str = 'postgres://{}:{}@{}:{}/{}'.format(config['postgres']['user'],
                                                           config['postgres']['password'],
                                                           config['postgres']['host'],
                                                           config['postgres']['port'],
                                                           config['postgres']['database'])
    await db.set_bind(bind_address)

    bloomfilter_sql_values: list = [
        '2',
        mock_bloomfilter.capacity,
        mock_bloomfilter.error_rate,
        mock_bloomfilter.bitarray.to01(),
        mock_bloomfilter.count,
    ]

    async with db.transaction():
        init_queries: list = [
            "INSERT INTO namespaces (name) VALUES ('empty');",
            "INSERT INTO namespaces (name) VALUES ('exists');",
            (
                "INSERT INTO bloomfilters (namespace_id, capacity, error_rate, bitarray, used) "
                "VALUES ('{}', '{}', '{}', '{}', '{}');".format(*bloomfilter_sql_values)
            ),
            (
                "INSERT INTO users (login, passwd, is_superuser, disabled) "
                "VALUES ('user', 'password', false, false);"
             )
        ]

        for query in init_queries:
            await db.status(query)

    yield db

    async with db.transaction():
        truncate_queries: list = [
            'TRUNCATE bloomfilters RESTART IDENTITY CASCADE;',
            'TRUNCATE namespaces RESTART IDENTITY CASCADE;',
            'TRUNCATE users RESTART IDENTITY CASCADE;',
        ]

        for query in truncate_queries:
            await db.status(query)

    await db.pop_bind().close()
