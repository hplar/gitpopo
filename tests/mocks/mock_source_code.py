#!/usr/bin/env python3


async def mock_function() -> None:
    username = 'John Doe'
    password = 'secret_password'

    print(username, password)


class MockClass:
    '''Single-line docstrings are supported by tokenize library'''

    def __init__(self):
        self.password = 'secret_password'

    def print_secret_password(self) -> None:
        print(self.password)


def main():
    mc = MockClass()
    mc.print_secret_password()


if __name__ == '__main__':
    main()
