class MockClass:
    '''
    Multi-line docstrings are not supported by tokenize libraryr
    '''

    def __init__(self):
        self.password = 'secret_password'

    def print_secret_password(self) -> None:
        print(self.password)
