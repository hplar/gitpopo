import json


async def test_bloomfilters_list_endpoint_returns_a_list_of_bloomfilters(db, aiohttp_client, app):
    payload = {'namespace': 'exists'}
    headers = {'Content-Type': 'application/json'}

    client = await aiohttp_client(app)
    response = await client.get('/api/v1/bloomfilters', json=payload, headers=headers)

    assert response.status == 200
    assert await response.json() == {
        'data': [
            {
                'id': 1,
                'namespace': 'exists',
                'capacity': 1000,
                'error_rate': 0.01,
                'used': 2
            }
        ]
    }


async def test_bloomfilters_list_endpoint_returns_empty_list_when_no_bloomfilters_exist(aiohttp_client, app):
    client = await aiohttp_client(app)
    response = await client.get('/api/v1/bloomfilters')

    assert response.status == 200
    assert await response.json() == {'data': []}


async def test_bloomfilters_detail_endpoint_returns_bloomfilter_when_it_exists(db, aiohttp_client, app):
    client = await aiohttp_client(app)
    response = await client.get('/api/v1/bloomfilters/1')

    assert response.status == 200
    assert await response.json() == {
        'data': {
            'id': 1,
            'namespace': 'exists',
            'capacity': 1000,
            'error_rate': 0.01,
            'used': 2
        }
    }


async def test_bloomfilters_detail_endpoint_returns_404_when_bloomfilter_doesnt_exist(aiohttp_client, app):
    client = await aiohttp_client(app)
    response = await client.get('/api/v1/bloomfilters/100')

    assert response.status == 404
    assert await response.json() == {
        'error': {
            'code': 404,
            'message': 'BloomFilter not found'
        }
    }


async def test_bloomfilters_post_endpoint_returns_201_on_succesful_creation(aiohttp_client, app, db):
    client = await aiohttp_client(app)

    secrets = [str(i) for i in range(1000)]
    payload = {
        'namespace': 'empty',
        'secrets': secrets,
    }

    response = await client.post('/api/v1/bloomfilters', json=payload)

    assert response.status == 201
    assert await response.json() == {
        'data': {
            'namespace': 'empty',
            'id': 2,
            'capacity': 2000,
            'error_rate': 0.0001,
            'used': 1000,
        }
    }


async def test_bloomfilters_post_endpoint_returns_422_when_json_payload_is_wrong(aiohttp_client, app):
    payload = json.dumps({'unexpectedkey': 'unexpectedvalue'})
    client = await aiohttp_client(app)
    response = await client.post('/api/v1/bloomfilters', json=payload)

    assert response.status == 422
    assert await response.json() == {
        'error': {
            'message': 'Namespace and secrets are required fields',
            'code': 422
        }
    }
