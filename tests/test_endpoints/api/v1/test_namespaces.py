import json


async def test_namespaces_list_endpoint_returns_a_list_of_namespaces(db, aiohttp_client, app):
    client = await aiohttp_client(app)
    response = await client.get('/api/v1/namespaces')

    assert response.status == 200
    assert await response.json() == {
        'data': [
            {'id': 1, 'name': 'empty'},
            {'id': 2, 'name': 'exists'}
        ]
    }


async def test_namespaces_list_endpoint_returns_empty_list_when_no_namespaces_exist(aiohttp_client, app):
    client = await aiohttp_client(app)
    response = await client.get('/api/v1/namespaces')

    assert response.status == 200
    assert await response.json() == {'data': []}


async def test_namespaces_detail_endpoint_returns_namespace_when_it_exists(db, aiohttp_client, app):
    client = await aiohttp_client(app)
    response = await client.get('/api/v1/namespaces/1')

    assert response.status == 200
    assert await response.json() == {
        'data': {
            'id': 1,
            'name': 'empty'
        }
    }


async def test_namespaces_detail_endpoint_returns_404_when_resource_doesnt_exist(aiohttp_client, app):
    client = await aiohttp_client(app)
    response = await client.get('/api/v1/namespaces/100')

    assert response.status == 404
    assert await response.json() == {
        "error": {
            "code": 404,
            "message": "Namespace not found"
        }
    }


async def test_namespaces_create_endpoint_returns_422_when_json_payload_is_wrong(aiohttp_client, app):
    headers = {
        'Content-Type': 'application/json',
    }

    payload = json.dumps({'unexpectedkey': 'unexpectedvalue'})
    client = await aiohttp_client(app)
    response = await client.post('/api/v1/namespaces', headers=headers, data=payload)

    assert response.status == 422
    assert await response.json() == {
        "error": {
            "code": 422,
            "message": "Name is a required key"
        }
    }


async def test_namespaces_create_endpoint_returns_201_on_succesful_insertion(aiohttp_client, app, db):
    headers = {
        'Content-Type': 'application/json',
    }

    payload = json.dumps({'name': 'createme'})
    client = await aiohttp_client(app)
    response = await client.post('/api/v1/namespaces', headers=headers, data=payload)

    assert response.status == 201
    assert await response.json() == {
        'data': {
            'id': 3,
            'name': 'createme'
        }
    }
    assert response.headers['Location'] == f'{response.url}/3'


async def test_namespaces_create_endpoint_returns_409_when_namespace_already_exists(aiohttp_client, app, db):
    headers = {
        'Content-Type': 'application/json',
    }

    payload = json.dumps({'name': 'exists'})
    client = await aiohttp_client(app)
    response = await client.post('/api/v1/namespaces', headers=headers, data=payload)

    assert response.status == 409
    assert await response.json() == {
        "error": {
            "code": 409,
            "message": "Namespace exists"
        }
    }
