async def test_scanner_returns_line_numbers_when_tokens_match_to_secrets(app, aiohttp_client, db):
    payload = {
        'namespace': 'exists',
        'tokens': [
            {'string': 's3CrT', 'line': 49},
        ]
    }

    client = await aiohttp_client(app)
    response = await client.post('/api/v1/scanner', json=payload)

    assert response.status == 200
    assert await response.json() == {
        'data': [
            {'line_nr': 49},
        ]
    }


async def test_scanner_endpoint_returns_empty_list_when_there_are_no_matches(app, aiohttp_client, db):
    payload = {
        'namespace': 'exists',
        'tokens': [
            {'string': 'NotASecret', 'line': 36},
            {'string': 'ALsoNotASecret', 'line': 17},
        ]
    }

    client = await aiohttp_client(app)
    response = await client.post('/api/v1/scanner', json=payload)

    assert response.status == 200
    assert await response.json() == {
        'data': []
    }


async def test_scanner_endpoint_returns_422_when_incorrect_json_payload_is_posted(app, aiohttp_client):
    payload = {
        'bad_key': [
            'some_string',
            'something',
            'secret',
        ]
    }

    client = await aiohttp_client(app)
    response = await client.post('/api/v1/scanner', json=payload)

    assert response.status == 422
    assert await response.json() == {
        'error': {
            'message': 'Namespace and tokens are required fields',
            'code': 422
        }
    }


async def test_scanner_endpoint_returns_422_when_no_json_payload_is_posted(app, aiohttp_client):
    client = await aiohttp_client(app)
    response = await client.post('/api/v1/scanner')

    assert response.status == 422
    assert await response.json() == {
        'error': {
            'message': 'Namespace and tokens are required fields',
            'code': 422
        }
    }
