from uuid import uuid4

from gitpopo.database import UserModel
from gitpopo.utils import check_credentials, hash_password


async def test_hash_password():
    for _ in range(3):
        password = str(uuid4())
        hashed_psw = await hash_password(password)
        assert len(hashed_psw) == 60
        assert hashed_psw.startswith('$2b$12')


async def test_check_credentials_returns_true_with_correct_password(db):
    password = str(uuid4())

    user = UserModel()
    user.passwd = await hash_password(password)

    assert await check_credentials(password, user)


async def test_check_credentials_returns_false_with_incorrect_password(db):
    password = str(uuid4())

    user = UserModel()
    user.passwd = await hash_password(password)

    assert not await check_credentials('thisisnotthepassword', user)
