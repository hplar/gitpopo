#GitPopo

Policing all your secrets in git.  

***
**REQUIREMENTS**
```    
python 3.8.2
postgresql-client
docker
docker-compose
```

***  
**SETUP/RUN**
  
```
git clone git@git.elements.nl:ralph.lamballais/gitpopo.git && 
cd gitpopo &&
make stack/up &&
make venv &&
source venv/bin/activate &&
make admin &&
make serve 
```
  
***
**USE**
  
WebGUI
```
localhost:8080
```
  
1. Login with admin user
2. Create a namespace
3. Create a bloomfilter with secrets (space separated)
  
---
Scanner Endpoint
  
```
curl -XPOST http://localhost:8080/api/v1/scanner -d @testdata.json -H "Content-Type: application/json"
```
  
---  
Swagger docs
```
localhost:8080/api/docs
```
