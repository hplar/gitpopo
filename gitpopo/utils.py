import bcrypt
from aiohttp_security import authorized_userid
from bitarray import bitarray
from gitpopo.database import BloomFilterModel
from pybloom_live import BloomFilter


async def hash_password(passwd: str) -> str:
    hashed_psw: str = bcrypt.hashpw(passwd.encode('utf-8'), bcrypt.gensalt(12)).decode()

    return hashed_psw


async def check_credentials(passwd: str, user) -> bool:
    if user is None or not bcrypt.checkpw(passwd.encode('utf-8'), user.passwd.encode('utf-8')):
        return False

    return True


async def current_user_ctx_processor(request) -> dict:
    """Get user context for use in jinja templates"""

    userid: str = await authorized_userid(request)
    is_anonymous: bool = not bool(userid)

    return {'current_user': {'is_anonymous': is_anonymous}}


async def reconstruct_bloomfilter(namespace_id) -> BloomFilter:
    """Reconstruct a bloomfilter object from a bloomfilter table row"""

    bloomfilter_model: BloomFilterModel = await BloomFilterModel.query.where(
        BloomFilterModel.namespace_id == namespace_id
    ).gino.first()

    if not bloomfilter_model:
        return None

    capacity: int = int(bloomfilter_model.capacity)
    error_rate: float = float(bloomfilter_model.error_rate)
    bit_array: bitarray = bloomfilter_model.bitarray
    used: int = bloomfilter_model.used

    bloomfilter: BloomFilter = BloomFilter(
        capacity=capacity,
        error_rate=error_rate,
    )

    bloomfilter.bitarray = bit_array
    bloomfilter.count = used

    return bloomfilter
