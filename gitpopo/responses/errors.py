import aiohttp


async def validation_error(msg='Validation Error'):
    response = aiohttp.web.json_response({
        'error': {
            'code': 422,
            'message': msg,
        }
    }, status=422)

    return response


async def not_found(msg='Not found'):
    response = aiohttp.web.json_response({
        'error': {
            'code': 404,
            'message': msg,
        }
    }, status=404)

    return response


async def conflict(msg='Conflict', status=409):
    response = aiohttp.web.json_response({
        'error': {
            'code': 409,
            'message': msg,
        }
    }, status=409)

    return response
