import aiohttp
from gitpopo.server_common import build_application


# Used by aiohttp_devtools runserver
async def create_app() -> aiohttp.web_app.Application:
    return await build_application()
