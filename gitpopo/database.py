import gino
from gino.ext.aiohttp import Gino

db: gino.api.Gino = Gino()


class PermissionsModel(db.Model):
    __tablename__ = 'permissions'

    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(None, db.ForeignKey('users.id'))
    perm_name = db.Column(db.Unicode())


class BloomFilterModel(db.Model):
    __tablename__ = 'bloomfilters'

    id = db.Column(db.Integer(), primary_key=True)
    bitarray = db.Column(db.Binary())
    capacity = db.Column(db.Integer())
    error_rate = db.Column(db.NUMERIC(precision=5))
    used = db.Column(db.Integer())
    namespace_id = db.Column(db.Integer(), db.ForeignKey('namespaces.id'))


class NamespaceModel(db.Model):
    __tablename__ = 'namespaces'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.Unicode())


class UserModel(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer(), primary_key=True)
    login = db.Column(db.Unicode())
    passwd = db.Column(db.Unicode())
    is_superuser = db.Column(db.Boolean())
    disabled = db.Column(db.Boolean(), default=False)
    identity = db.Column(db.Unicode())
    namespace_id = db.Column(db.Integer(), db.ForeignKey('namespaces.id'))
