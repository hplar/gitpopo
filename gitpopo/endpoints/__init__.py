from gitpopo.endpoints.api.v1.bloomfilters import bloomfilters_endpoint
from gitpopo.endpoints.api.v1.namespaces import namespaces_endpoint
from gitpopo.endpoints.api.v1.scanner import scanner_endpoint

api: list = [
    scanner_endpoint,
    namespaces_endpoint,
    bloomfilters_endpoint,
]
