import typing
from collections import OrderedDict

import aiohttp
import asyncpg
from aiohttp_apispec import docs, json_schema
from gitpopo.database import NamespaceModel
from gitpopo.responses import errors
from gitpopo.schemas import http as http_schemas
from gitpopo.schemas import namespaces as namespaces_schemas

namespaces_endpoint = aiohttp.web.RouteTableDef()


@docs(
    tags=['Namespaces'],
    summary='Retreive namespaces list',
    description='Retrieve a list of all namespaces',
    responses={
        200: {'description': 'Ok', 'schema': namespaces_schemas.NamespacesListSchema},
        404: {'description': 'Namespace not found', 'schema': http_schemas.ResourceNotFoundSchema},
    },
)
@namespaces_endpoint.get('/api/v1/namespaces')
async def get_namespaces(request: aiohttp.web_request.Request) -> aiohttp.web.Response:
    namespace_models: typing.List[NamespaceModel, ] = await NamespaceModel.query.gino.all()
    namespaces: list = []

    for namespace in namespace_models:
        namespaces.append(OrderedDict({
            'id': namespace.id,
            'name': namespace.name
        }))

    response: dict = {'data': namespaces}
    return aiohttp.web.json_response(response, status=200)


@docs(
    tags=['Namespaces'],
    summary='Retrieve namespaces detail',
    description='Retrieve a single namespace',
    responses={
        200: {'description': 'Ok', 'schema': namespaces_schemas.NamespacesDetailSchema},
        404: {'description': 'Namespace not found', 'schema': http_schemas.ResourceNotFoundSchema},
    },
)
@namespaces_endpoint.get('/api/v1/namespaces/{id}')
async def get_namespace(request: aiohttp.web_request.Request) -> aiohttp.web.Response:
    request_namespace_id: int = int(request.match_info['id'])
    namespace_model: NamespaceModel = await NamespaceModel.get(request_namespace_id)

    if not namespace_model:
        return await errors.not_found(msg='Namespace not found')

    namespace_dict: OrderedDict = OrderedDict({
        'id': namespace_model.id,
        'name': namespace_model.name
    })

    response: dict = {'data': namespace_dict}
    return aiohttp.web.json_response(response, status=200)


@docs(
    tags=['Namespaces'],
    summary='Create namespaces',
    description='Create a new namespaces',
    responses={
        201: {'description': 'Created', 'schema': namespaces_schemas.NamespaceCreatedSchema},
        409: {'description': 'Conflict', 'schema': http_schemas.ConflictSchema},
        422: {'description': 'Validation error', 'schema': http_schemas.ValidationErrorSchema},
    },
)
@json_schema(namespaces_schemas.NamespacePostSchema())
@namespaces_endpoint.post('/api/v1/namespaces')
async def post(request: aiohttp.web_request.Request) -> aiohttp.web.Response:
    json_dict: dict = request['json']

    try:
        request_name: str = json_dict['name']
    except KeyError:
        return await errors.validation_error(msg='Name is a required key')

    namespace_model: NamespaceModel = NamespaceModel(name=request_name)

    try:
        await namespace_model.create()
    except asyncpg.exceptions.UniqueViolationError:
        return await errors.conflict(msg='Namespace exists')

    uri: str = f'{request.url}/{namespace_model.id}'
    headers: dict = {'Location': uri}

    response: dict = {
        'data': {
            'id': namespace_model.id,
            'name': namespace_model.name
        }
    }

    return aiohttp.web.json_response(response, status=201, headers=headers)
