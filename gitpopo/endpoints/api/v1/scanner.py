import aiohttp
from aiohttp_apispec import docs, json_schema
from gitpopo.database import NamespaceModel
from gitpopo.responses import errors
from gitpopo.scanner import Scanner
from gitpopo.schemas import http as http_schemas
from gitpopo.schemas import scanner as scanner_schemas
from gitpopo.utils import reconstruct_bloomfilter
from pybloom_live import BloomFilter

scanner_endpoint = aiohttp.web.RouteTableDef()


@docs(
    tags=['Scanner'],
    summary='Scan for secrets',
    description='Scan a list of tokens against bloomfilters in a namespace to determine if they are known secrets',
    responses={
        200: {'description': 'Ok', 'schema': scanner_schemas.ScannerPostResponseSchema},
        404: {'description': 'Namespace not found', 'schema': http_schemas.ResourceNotFoundSchema},
        422: {'description': 'Validation error', 'schema': http_schemas.ResourceNotFoundSchema},
    },
)
@json_schema(scanner_schemas.ScannerPostRequestSchema)
@scanner_endpoint.post('/api/v1/scanner')
async def scanner(request: aiohttp.web_request.Request) -> aiohttp.web.Response:
    '''
    This endpoint provides the main functionality of GitPopo.
    It takes a namespace and a list of tokens (dictionaries) and checks
    tokens against the bloomfilter related to the namespace.

    Example request payload:

    {
        "namespace": "foo",
        "tokens": [
            {"string": "bar", "line": 5},
            {"string": "baz", "line": 8},
        ]
    }

    Example response payload:

    {
        "data": {
            {"line": 8},
        }
    }
    '''
    json_dict: dict = request['json']

    try:
        request_namespace = json_dict['namespace']
        tokens = json_dict['tokens']
    except KeyError:
        return await errors.validation_error(msg='Namespace and tokens are required fields')

    namespace: NamespaceModel = await NamespaceModel.query.where(
        NamespaceModel.name == request_namespace
    ).gino.first()

    if not namespace:
        return await errors.not_found(msg='Namespace not found')

    bloomfilter: BloomFilter = await reconstruct_bloomfilter(namespace.id)

    if not bloomfilter:
        return await errors.not_found(msg='No bloomfilters in this namespace')

    scanner: Scanner = Scanner(bloomfilter=bloomfilter)
    matches: list = await scanner.scan(tokens)

    response: dict = {
        'data': matches
    }

    return aiohttp.web.json_response(response, status=200)
