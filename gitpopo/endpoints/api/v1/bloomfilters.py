import typing

import aiohttp
from aiohttp_apispec import docs, json_schema
from asyncpg.exceptions import UniqueViolationError
from gitpopo.database import BloomFilterModel, NamespaceModel
from gitpopo.responses import errors
from gitpopo.schemas import bloomfilters as bloomfilter_schemas
from gitpopo.schemas import http as http_schemas
from pybloom_live import BloomFilter

bloomfilters_endpoint = aiohttp.web.RouteTableDef()


@docs(
    tags=['BloomFilters'],
    summary='BloomFilters list view',
    description='Retrieve bloomfilters by namespace',
    responses={
        200: {'description': 'Ok', 'schema': bloomfilter_schemas.BloomFilterListResponseSchema},
        404: {'description': 'Resource not found', 'schema': http_schemas.ResourceNotFoundSchema},
    },
)
@bloomfilters_endpoint.get('/api/v1/bloomfilters', allow_head=False)
async def get_list(request: aiohttp.web_request.Request):
    bloomfilters: typing.List[dict, ] = []
    bloomfilter_models: BloomFilterModel = await BloomFilterModel.query.gino.all()

    for bloomfilter_model in bloomfilter_models:
        namespace_model: NamespaceModel = await NamespaceModel.query.where(
            NamespaceModel.id == bloomfilter_model.namespace_id
        ).gino.first()

        bloomfilters.append({
            'id': bloomfilter_model.id,
            'capacity': bloomfilter_model.capacity,
            'error_rate': float(bloomfilter_model.error_rate),
            'used': bloomfilter_model.used,
            'namespace': namespace_model.name,
        })

    response: dict = {'data': bloomfilters}
    return aiohttp.web.json_response(response, status=200)


@docs(
    tags=['BloomFilters'],
    summary='BloomFilters detail view',
    description='Retrieve a single bloomfilter',
    responses={
        200: {'description': 'Ok', 'schema': bloomfilter_schemas.BloomFilterDetailSchema},
        404: {'description': 'Resource not found', 'schema': http_schemas.ResourceNotFoundSchema},
    },
)
@bloomfilters_endpoint.get('/api/v1/bloomfilters/{id}', allow_head=False)
async def get_detail(request: aiohttp.web_request.Request) -> aiohttp.web.Response:
    request_bloomfilter_id = int(request.match_info.get('id'))
    bloomfilter_model: BloomFilterModel = await BloomFilterModel.get(request_bloomfilter_id)

    if not bloomfilter_model:
        return await errors.not_found(msg='BloomFilter not found')

    namespace_model: NamespaceModel = await NamespaceModel.query.where(
        NamespaceModel.id == bloomfilter_model.namespace_id
    ).gino.first()

    bloomfilter: dict = {
        'id': bloomfilter_model.id,
        'capacity': bloomfilter_model.capacity,
        'error_rate': float(bloomfilter_model.error_rate),
        'used': bloomfilter_model.used,
        'namespace': namespace_model.name,
    }

    response: dict = {'data': bloomfilter}

    return aiohttp.web.json_response(response, status=200)


@docs(
    tags=['BloomFilters'],
    summary='Create bloomfilters',
    description='Create a bloomfilter in a namespace',
    responses={
        201: {'description': 'Created', 'schema': bloomfilter_schemas.BloomFilterPostResponseSchema},
        409: {'description': 'Conflict', 'schema': http_schemas.ConflictSchema},
        422: {'description': 'Validation error', 'schema': http_schemas.ValidationErrorSchema},
    },
)
@json_schema(bloomfilter_schemas.BloomFilterPostRequestSchema())
@bloomfilters_endpoint.post('/api/v1/bloomfilters')
async def post(request: aiohttp.web_request.Request) -> aiohttp.web.Response:
    json_dict: dict = request['json']

    try:
        request_namespace: str = json_dict['namespace']
        request_secrets: list = json_dict['secrets']
    except KeyError:
        return await errors.validation_error('Namespace and secrets are required fields')

    namespace_model: NamespaceModel = await NamespaceModel.query.where(
        NamespaceModel.name == request_namespace
    ).gino.first()

    if not namespace_model:
        return await errors.not_found(msg='Namespace not found')

    bloomfilter: BloomFilter = BloomFilter(capacity=2000,
                                           error_rate=0.0001)

    for secret in request_secrets:
        bloomfilter.add(secret)

    bloomfilter_model: BloomFilterModel = BloomFilterModel(bitarray=bloomfilter.bitarray,
                                                           capacity=bloomfilter.capacity,
                                                           error_rate=bloomfilter.error_rate,
                                                           used=bloomfilter.count,
                                                           namespace_id=namespace_model.id)

    try:
        await bloomfilter_model.create()
    except UniqueViolationError:
        return errors.conflict(msg='Uniqe violation error', status=409)

    bloomfilter_dict: dict = {
        'id': bloomfilter_model.id,
        'namespace': namespace_model.name,
        'capacity': bloomfilter_model.capacity,
        'error_rate': float(bloomfilter_model.error_rate),
        'used': bloomfilter_model.used
    }

    response: dict = {'data': bloomfilter_dict}

    return aiohttp.web.json_response(response, status=201)
