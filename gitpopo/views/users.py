import typing

import aiohttp
import multidict
from aiohttp_apispec import form_schema
from aiohttp_jinja2 import template
from aiohttp_security import check_permission
from asyncpg.exceptions import UniqueViolationError
from gitpopo.database import NamespaceModel, PermissionsModel, UserModel
from gitpopo.schemas.forms.users import Create
from gitpopo.utils import hash_password

users_view: aiohttp.web_routedef.RouteTableDef = aiohttp.web.RouteTableDef()


@users_view.view('/users')
class UserListView(aiohttp.web.View):

    @template('users.jinja2')
    async def get(self):
        '''Get list of users'''
        await check_permission(self.request, 'admin')

        user_models: list = await UserModel.query.gino.all()
        namespace_model: NamespaceModel = await NamespaceModel.query.gino.all()

        return {'userspage': True, 'users': user_models, 'namespaces': namespace_model}

    @form_schema(Create)
    @template('users.jinja2')
    async def post(self):
        '''Create a new user'''
        await check_permission(self.request, 'admin')

        form: dict = self.request['form']

        login: str = form['login']
        passwd: str = form['passwd']
        passwd_repeat: str = form['passwdrepeat']
        namespace: str = form['namespace']
        superuser: bool = bool(form.get('superuser'))

        if passwd != passwd_repeat:
            return {
                'warning': 'Passwords don\'t match',
            }

        hashed_passwd: str = await hash_password(passwd)

        namespace_model: NamespaceModel = await NamespaceModel.query.where(
            NamespaceModel.name == namespace
        ).gino.first()

        permission_name: str = 'user'

        if superuser:
            permission_name = 'admin'

        user_model: UserModel = UserModel(
            login=login,
            passwd=hashed_passwd,
            is_superuser=superuser,
            namespace_id=namespace_model.id,
        )

        try:
            await user_model.create()
        except UniqueViolationError:
            return aiohttp.web.HTTPConflict()

        permissions: PermissionsModel = PermissionsModel(
            user_id=user_model.id,
            perm_name=permission_name
        )

        await permissions.create()
        raise aiohttp.web.HTTPFound('/users')


@users_view.view('/users/{id}')
class UserDetailView(aiohttp.web.View):

    @template('user_detail.jinja2')
    async def get(self) -> dict:
        '''Get user details'''
        await check_permission(self.request, 'admin')

        user_model: UserModel = await UserModel.query.where(
            UserModel.id == int(self.request.match_info['id'])
        ).gino.first()

        namespace_models: typing.List[NamespaceModel, ] = await NamespaceModel.query.gino.all()

        template_context: dict = {
            'user': user_model,
            'namespaces': namespace_models
        }

        return template_context

    @template('user_detail.jinja2')
    async def post(self) -> aiohttp.web.Response:
        await check_permission(self.request, 'admin')

        form: multidict.MultiDictProxy = await self.request.post()

        if form['method'] == 'delete':
            return await self._delete(form)

        return await self._update(form)

    async def _update(self, form) -> aiohttp.web.Response:
        '''Update user'''
        request_user_id: int = int(self.request.match_info['id'])
        request_namespace_name: str = form['namespace']

        user_model: UserModel = await UserModel.get(request_user_id)
        namespace_model: NamespaceModel = await NamespaceModel.query.where(
            NamespaceModel.name == request_namespace_name
        ).gino.first()

        await user_model.update(namespace_id=namespace_model.id).apply()

        return aiohttp.web.HTTPFound(f'/users/{user_model.id}')

    async def _delete(self, form) -> aiohttp.web.Response:
        '''Delete user'''
        user: UserModel = await UserModel.get(int(self.request.match_info['id']))

        await user.delete()

        return aiohttp.web.HTTPFound('/users')
