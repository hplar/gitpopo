from gitpopo.views.bloomfilters import bloomfilters_view
from gitpopo.views.index import index_view
from gitpopo.views.login import login_view
from gitpopo.views.logout import logout_view
from gitpopo.views.namespaces import namespaces_view
from gitpopo.views.users import users_view

views: list = [
    bloomfilters_view,
    index_view,
    login_view,
    logout_view,
    namespaces_view,
    users_view,
]
