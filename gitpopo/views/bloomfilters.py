import aiohttp
import multidict
from aiohttp_apispec import form_schema
from aiohttp_jinja2 import template
from aiohttp_security import check_permission
from asyncpg.exceptions import UniqueViolationError
from gitpopo.database import BloomFilterModel, NamespaceModel
from gitpopo.schemas.forms.bloomfilters import Create
from pybloom_live import BloomFilter

bloomfilters_view: aiohttp.web_routedef.RouteTableDef = aiohttp.web.RouteTableDef()


@bloomfilters_view.view('/bloomfilters')
class BloomFiltersView(aiohttp.web.View):

    @template('bloomfilter_list.jinja2')
    async def get(self) -> dict:
        '''List bloomfilters'''
        await check_permission(self.request, 'user')

        namespaces: NamespaceModel = await NamespaceModel.query.gino.all()
        bloomfilters: multidict.MultiDictProxy = await BloomFilterModel.query.gino.all()

        return {
            'filterspage': True,
            'namespaces': namespaces,
            'bloomfilters': bloomfilters
        }

    @form_schema(Create)
    @template('bloomfilter_list.jinja2')
    async def post(self):
        '''Create bloomfilters'''
        await check_permission(self.request, 'admin')

        form: dict = self.request['form']

        secrets: list[str, ] = form['secrets'].split()
        namespace_name: str = form['namespace']

        namespace_model: NamespaceModel = await NamespaceModel.query.where(
            NamespaceModel.name == namespace_name
        ).gino.first()

        bloomfilter = BloomFilter(
            capacity=2000,
            error_rate=0.0001,
        )

        for secret in secrets:
            bloomfilter.add(secret)

        bloomfilter_model: BloomFilterModel = BloomFilterModel(
            namespace_id=namespace_model.id,
            capacity=bloomfilter.capacity,
            error_rate=bloomfilter.error_rate,
            bitarray=bloomfilter.bitarray,
            used=bloomfilter.count,
        )

        try:
            await bloomfilter_model.create()
            raise aiohttp.web.HTTPFound('/bloomfilters')
        except UniqueViolationError:
            raise aiohttp.web.HTTPConflict()


@bloomfilters_view.view('/bloomfilters/{id}')
class BloomFiltersDetailView(aiohttp.web.View):

    @template('bloomfilter_detail.jinja2')
    async def get(self) -> dict:
        await check_permission(self.request, 'user')

        request_bloomfilter_id: int = int(self.request.match_info['id'])
        bloomfilter_model: BloomFilterModel = await BloomFilterModel.get(request_bloomfilter_id)

        return {'bloomfilter': bloomfilter_model}

    @template('bloomfilter_list.jinja2')
    async def post(self):
        await check_permission(self.request, 'admin')

        request_bloomfilter_id: int = int(self.request.match_info['id'])
        bloomfilter_model: BloomFilterModel = await BloomFilterModel.get(request_bloomfilter_id)
        await bloomfilter_model.delete()

        raise aiohttp.web.HTTPFound('/bloomfilters')
