import aiohttp
from aiohttp_jinja2 import template

index_view: aiohttp.web_routedef.RouteTableDef = aiohttp.web.RouteTableDef()


@index_view.view('/')
class IndexView(aiohttp.web.View):

    @template('index.jinja2')
    async def get(self):
        return
