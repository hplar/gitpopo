import uuid

import aiohttp
from aiohttp_apispec import form_schema
from aiohttp_jinja2 import template
from aiohttp_security import remember
from gitpopo.database import UserModel
from gitpopo.schemas.forms.login import Login as LoginSchema
from gitpopo.utils import check_credentials

login_view: aiohttp.web_routedef.RouteTableDef = aiohttp.web.RouteTableDef()


@login_view.view('/login')
class LoginView(aiohttp.web.View):

    @template('login.jinja2')
    async def get(self) -> dict:
        '''Display the login page'''
        return {}

    @form_schema(LoginSchema)
    @template('login.jinja2')
    async def post(self):
        '''Authenticate user and session identity'''
        form: dict = self.request['form']
        login: str = form.get('login', '')
        passwd: str = form.get('passwd', '')

        user_model: UserModel = await UserModel.query.where(
            UserModel.login == login and not UserModel.disabled
        ).gino.first()

        if not await check_credentials(passwd, user_model):
            return {
                'error': 'Unknown login/password combination',
            }

        response: aiohttp.web_exceptions.HTTPFound = aiohttp.web.HTTPFound('/')
        identity_uuid: str = str(uuid.uuid4())

        await remember(self.request, response, identity_uuid)
        await user_model.update(identity=identity_uuid).apply()
        raise response
