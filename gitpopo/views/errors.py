from aiohttp_jinja2 import render_template


async def handle_401(request):
    return render_template('errors/401.jinja2', request, {})


async def handle_403(request):
    return render_template('errors/403.jinja2', request, {})


async def handle_404(request):
    return render_template('errors/404.jinja2', request, {})


async def handle_409(request):
    return render_template('errors/409.jinja2', request, {'error': 'Resource exists'})
