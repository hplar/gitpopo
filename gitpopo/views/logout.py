import aiohttp
from aiohttp_jinja2 import template
from aiohttp_security import check_authorized, forget

logout_view: aiohttp.web_routedef.RouteTableDef = aiohttp.web.RouteTableDef()


@logout_view.get('/logout')
class LogoutView(aiohttp.web.View):

    @template('logout.jinja2')
    async def get(self) -> None:
        '''Remove user session key from redis'''
        await check_authorized(self.request)

        response: aiohttp.web_exceptions.HTTPFound = aiohttp.web.HTTPFound('/')
        await forget(self.request, response)

        raise response
