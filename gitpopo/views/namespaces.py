import aiohttp
import multidict
from aiohttp_apispec import form_schema
from aiohttp_jinja2 import template
from aiohttp_security import check_permission
from asyncpg.exceptions import UniqueViolationError
from gitpopo.database import BloomFilterModel, NamespaceModel, UserModel
from gitpopo.schemas.forms.namespaces import Create

namespaces_view: aiohttp.web_routedef.RouteTableDef = aiohttp.web.RouteTableDef()


@namespaces_view.view('/namespaces')
class NamespaceListView(aiohttp.web.View):
    @template('namespaces.jinja2')
    async def get(self) -> dict:
        '''Return a list of all namespaces'''
        await check_permission(self.request, 'admin')

        namespace_models: multidict.MultiDictProxy = await NamespaceModel.query.gino.all()

        return {'namespaces': namespace_models}

    @form_schema(Create)
    @template('namespaces.jinja2')
    async def post(self) -> None:
        '''Create a namespace'''
        await check_permission(self.request, 'admin')

        form: multidict.MultiDictProxy = self.request['form']
        namespace_model: NamespaceModel = NamespaceModel(name=form['name'])

        try:
            await namespace_model.create()
        except UniqueViolationError:
            raise aiohttp.web.HTTPConflict()

        raise aiohttp.web.HTTPFound('/namespaces')


@namespaces_view.view('/namespaces/{id}')
class NameSpaceDetailView(aiohttp.web.View):
    @template('namespace_detail.jinja2')
    async def get(self) -> dict:
        '''Get the detail page of a namespace'''
        await check_permission(self.request, 'admin')

        request_namespace_id: int = int(self.request.match_info['id'])
        namespace_model: NamespaceModel = await NamespaceModel.get(request_namespace_id)

        users: UserModel = await UserModel.query.where(
            UserModel.namespace_id == namespace_model.id
        ).gino.all()

        bloomfilter: BloomFilterModel = await BloomFilterModel.query.where(
            BloomFilterModel.namespace_id == namespace_model.id
        ).gino.first()

        template_context: dict = {
            'namespace': namespace_model,
            'users': users,
            'bloomfilter': bloomfilter
        }

        return template_context

    @template('namespaces.jinja2')
    async def post(self) -> None:
        '''Delete a namespace'''
        await check_permission(self.request, 'admin')

        request_namespace_id: int = int(self.request.match_info['id'])
        namespace_model: NamespaceModel = await NamespaceModel.get(request_namespace_id)

        await namespace_model.delete()

        raise aiohttp.web.HTTPFound('/namespaces')
