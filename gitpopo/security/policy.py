from aiohttp_security.abc import AbstractAuthorizationPolicy
from gitpopo.database import PermissionsModel, UserModel


class AuthorizationPolicy(AbstractAuthorizationPolicy):
    async def authorized_userid(self, identity):
        user = await UserModel.query.where(UserModel.identity == identity and not UserModel.disabled).gino.all()

        if user:
            return identity
        else:
            return None

    async def permits(self, identity, permission, context=None):
        if identity is None:
            return False

        user = await UserModel.query.where(UserModel.identity == identity and not UserModel.disabled).gino.one()

        if user is not None:
            if user.is_superuser:
                return True

            perms = await PermissionsModel.query.where(PermissionsModel.user_id == user.id).gino.all()
            if perms:
                for perm in perms:
                    if perm.perm_name == permission:
                        return True

        return False
