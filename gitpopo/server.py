import aiohttp
from gitpopo.server_common import build_application

if __name__ == '__main__':
    app = build_application()
    aiohttp.web.run_app(app)
