from marshmallow import Schema, fields


class Create(Schema):
    login: fields.Str = fields.Str()
    passwd: fields.Str = fields.Str()
    passwdrepeat: fields.Str = fields.Str()
    namespace: fields.Str = fields.Str()
    superuser: fields.Str = fields.Str()


class Update(Schema):
    id: fields.Int = fields.Int()
    namespace: fields.Str = fields.Str()
