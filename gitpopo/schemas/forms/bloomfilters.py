from marshmallow import Schema, fields


class Create(Schema):
    namespace: fields.Str = fields.Str()
    capacity: fields.Int = fields.Int()
    error_rate: fields.Float = fields.Float()
    secrets: fields.Str = fields.Str()
