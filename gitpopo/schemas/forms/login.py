from marshmallow import Schema, fields


class Login(Schema):
    login: fields.Str = fields.Str()
    passwd: fields.Str = fields.Str()
