from marshmallow import Schema, fields


class Create(Schema):
    name: fields.Str = fields.Str()
