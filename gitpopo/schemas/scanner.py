from gitpopo.schemas.secrets import SecretRequestSchema, SecretResponseSchema
from marshmallow import Schema, fields


class ScannerMeta(Schema):
    namespace: fields.Str = fields.Str()
    bloomfilter: fields.Int = fields.Int()
    matches: fields.List = fields.List(fields.Str)


class ScannerPostRequestSchema(Schema):
    namespace: fields.Str = fields.Str()
    tokens: fields.List = fields.List(fields.Nested(SecretRequestSchema))


class ScannerPostResponseSchema(Schema):
    data: fields.List = fields.List(fields.Nested(SecretResponseSchema))
