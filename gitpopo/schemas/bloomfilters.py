
from marshmallow import Schema, fields


class BloomFilterMeta(Schema):
    id: fields.Int = fields.Int()
    capacity: fields.Int = fields.Int()
    error_rate: fields.Float = fields.Float()
    count: fields.Int = fields.Int()


class BloomFilterListResponseSchema(Schema):
    data: fields.List = fields.List(fields.Nested(BloomFilterMeta))


class BloomFilterDetailSchema(Schema):
    data: fields.Nested = fields.Nested(BloomFilterMeta)


class BloomFilterPostRequestSchema(Schema):
    namespace: fields.Str = fields.Str()
    capacity: fields.Int = fields.Int()
    error_rate: fields.Float = fields.Float()
    secrets: fields.List = fields.List(fields.Str)


class BloomFilterPostResponseSchema(Schema):
    data: fields.Nested = fields.Nested(BloomFilterMeta)
