from marshmallow import Schema, fields


class NamespaceSchema(Schema):
    id = fields.Int()
    name = fields.Str(description='Namespace name')


class NamespacesDetailSchema(Schema):
    data: fields.Nested = fields.Nested(NamespaceSchema)


class NamespacesListSchema(Schema):
    data: fields.List = fields.List(fields.Nested(NamespaceSchema))


class NamespaceCreatedSchema(Schema):
    data: fields.Nested = fields.Nested(NamespaceSchema)


class NamespacePostSchema(Schema):
    name: fields.Str = fields.Str()
