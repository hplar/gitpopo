from marshmallow import Schema, fields


class ErrorMessage(Schema):
    code: fields.Int = fields.Int()
    message: fields.Str = fields.Str()


class ResourceNotFoundSchema(Schema):
    error: fields.Nested = fields.Nested(ErrorMessage)


class ConflictSchema(Schema):
    error: fields.Nested = fields.Nested(ErrorMessage)


class ValidationErrorSchema(Schema):
    error: fields.Nested = fields.Nested(ErrorMessage)
