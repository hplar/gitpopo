from marshmallow import Schema, fields


class SecretRequestSchema(Schema):
    line: fields.Int = fields.Int()
    string: fields.Str = fields.Str()


class SecretResponseSchema(Schema):
    line: fields.Int = fields.Int()
