import typing

from pybloom_live import BloomFilter


class Scanner:
    def __init__(self, bloomfilter: BloomFilter) -> None:
        '''
        A class to run scans on tokens.

        Args:
            tokens (list): A list of tokens.
            bloomfilter (py_bloomlive.BloomFilter): A populated bloomfilter.
        '''
        self.bloomfilter: BloomFilter = bloomfilter
        self.matches: typing.List[dict] = []

    async def scan(self, tokens: list) -> typing.List[dict]:
        '''
        Compare every token string to values in the bloomfilter to determine if
        they are a secret.

        Returns:
            List of tokens.
        '''
        for token in tokens:
            if token['string'] in self.bloomfilter:
                self.matches.append({'line_nr': token['line']})

        return self.matches
