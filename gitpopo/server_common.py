import asyncio
import logging
import os

import aiohttp
import aiohttp.web
import aiohttp_jinja2
import jinja2
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware
from aiohttp_security import SessionIdentityPolicy
from aiohttp_security import setup as setup_security
from aiohttp_session import setup as setup_session
from aiohttp_session.redis_storage import RedisStorage
from aioredis import create_redis_pool
from dotenv import load_dotenv
from gitpopo import endpoints
from gitpopo import views
from gitpopo.database import db
from gitpopo.views import errors
from gitpopo.middlewares.error import create_error_middleware
from gitpopo.security.policy import AuthorizationPolicy
from gitpopo.utils import current_user_ctx_processor


async def configure() -> dict:
    load_dotenv('.env')

    config = {
        'redis': {
            'host': os.getenv('GITPOPO_REDIS_HOST'),
            'port': os.getenv('GITPOPO_REDIS_PORT')
        },
        'postgres': {
            'host': os.getenv('GITPOPO_DB_HOST'),
            'port': os.getenv('GITPOPO_DB_PORT'),
            'database': os.getenv('GITPOPO_DB_NAME'),
            'user': os.getenv('GITPOPO_DB_USER'),
            'password': os.getenv('GITPOPO_DB_PASSWORD'),
        }
    }

    return config


async def make_app(loop) -> aiohttp.web.Application:
    config = await configure()

    # add custom error middlewares
    error_middlware = create_error_middleware({
        401: errors.handle_401,
        403: errors.handle_403,
    })

    # configure middlewares
    middlewares = [db, error_middlware, validation_middleware]

    # create app
    aiohttp_app = aiohttp.web.Application(middlewares=middlewares)

    # create redis pool
    redis_pool = await create_redis_pool((config['redis']['host'],
                                          config['redis']['port']),
                                         minsize=5, maxsize=100)

    storage = RedisStorage(redis_pool, max_age=600)
    setup_session(aiohttp_app, storage)

    # configure Gino db connection
    db.init_app(aiohttp_app, config=config['postgres'])

    # configure security
    setup_security(aiohttp_app,
                   SessionIdentityPolicy(),
                   AuthorizationPolicy())

    # configure aiohttp_apispec
    setup_aiohttp_apispec(
        app=aiohttp_app,
        title='My Documentation',
        version='v1',
        url='/api/docs/swagger.json',
        swagger_path='/api/docs',
    )

    # add routes
    for view in views.views:
        aiohttp_app.add_routes(view)

    for endpoint in endpoints.api:
        aiohttp_app.add_routes(endpoint)

    aiohttp_app.add_routes([aiohttp.web.static('/', 'gitpopo/static')])

    # configure jinja2 support
    aiohttp_jinja2.setup(
        aiohttp_app,
        context_processors=[current_user_ctx_processor],
        loader=jinja2.FileSystemLoader('gitpopo/jinja2')
    )

    return aiohttp_app


async def build_application(loop=None) -> aiohttp.web_app.Application:
    logging.basicConfig(level=logging.INFO)

    if not loop:
        loop = asyncio.get_event_loop()

    app: aiohttp.web_app.Application = await make_app(loop)

    return app
