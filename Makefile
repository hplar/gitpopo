clean/python:
	@find gitpopo -type d -name "__pycache__" -exec rm -rf {} \;
	@find gitpopo -type f -name "*.pyc" -exec rm -rf {} \;
	@rm -rf .mypy_cache
	@rm -rf .pytest_cache
	@rm -rf .coverage
	@echo "done"

stack/up:
	docker-compose up -d postgres redis
	docker ps

stack/down:
	docker-compose kill
	docker ps

stack/clean:
	docker rm gitpopo_postgres_1 gitpopo_redis_1

stack/refresh: stack/dev/down stack/dev/clean stack/dev/up

venv: venv/create venv/install

venv/create:
	python3 -m venv venv

venv/install:
	./venv/bin/pip3 install -r requirements/base.txt

clean/venv:
	rm -rf venv

venv/dev:
	python3 -m venv venv
	./venv/bin/pip3 install -r requirements/base.txt -r requirements/testing.txt -r requirements/dev.txt

venv/dev/refresh:
	python3 -m venv venv
	./venv/bin/pip3 install -r requirements/base.txt -r requirements/testing.txt -r requirements/dev.txt

lint/all:
	tox -e flake8 -e isort-check -e isort-fix -e mypy 

mypy:
	tox -e mypy

isort:
	tox -e isort-check -e isort-fix

flake8:
	tox -e flake8

test:
	tox -e py38

testdb:
	docker run --rm --name gitpopo_postgres -e POSTGRES_DB=test -e POSTGRES_USER=test -e POSTGRES_PASSWORD=test -d -p 5432:5432 postgres

testdb/init:
	PGPASSWORD=test psql -U test -h localhost -p 5432 -d test -f ./sql/init_postgres.sql

serve:
	PYTHONPATH=. python3 ./gitpopo/server.py

serve/dev:
	adev runserver gitpopo/server_dev.py

admin:
	./create_superuser.py

docker/image:
	DOCKER_BUILDKIT=1 docker build --tag gitpopo .

tags:
	ctags --exclude="venv" --exclude=".tox" -R .
