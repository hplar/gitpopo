#!/usr/bin/env python3


"""
Create a GitPopo user with admin privileges.
Requires psql and $PGPASSOWRD environment variable to work.
"""


from getpass import getpass
import bcrypt
from subprocess import Popen
from time import sleep
import uuid


def run():
    login = input('username: ')
    passwd = getpass('password: ')
    identity = str(uuid.uuid4())

    hashed_passwd = bcrypt.hashpw(passwd.encode(), bcrypt.gensalt(12)).decode()

    usr_sql = (f"INSERT INTO users (login, passwd, is_superuser)"
               f"VALUES ('{login}', '{hashed_passwd}', true);")

    perm_sql = (f"INSERT INTO permissions (user_id, perm_name) "
                f"VALUES (1, 'admin');")

    Popen(['docker', 'exec', 'gitpopo_postgres_1', 'psql', '-h', 'localhost', '-p', '5432', 'gitpopo', '-U', 'gitpopo', '-c', usr_sql])
    sleep(1)
    Popen(['docker', 'exec', 'gitpopo_postgres_1', 'psql', '-h', 'localhost', '-p', '5432', 'gitpopo', '-U', 'gitpopo', '-c', perm_sql])


if __name__ == '__main__':
    run()
